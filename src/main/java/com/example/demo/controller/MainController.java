package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/api")
public class MainController {
    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
    public String greeting() {
        return "Hi Ms. Taing Y";
    }
}
